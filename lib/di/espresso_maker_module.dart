import 'package:flutter_playground_tdd/domain/logic.dart';
import 'package:flutter_playground_tdd/domain/make_espresso_use_case.dart';
import 'package:flutter_playground_tdd/domain/models.dart';

import 'core.dart';

class EspressoMakerModule extends Module {
  MakeEspressoUseCase get makeEspressoUseCase => get();

  @override
  void provideInstances() {
    provideSingleton(() => MakeEspressoUseCase(
        waterContainer: get(), coffeeBeansContainer: get(), heater: get(),grinder: get()));
    provideSingleton(() => WaterContainer(
        capacity: 1000,
        currentLoad: 1000,
        dispense: (amount) => Water.roomTemperature(amount: amount)));
    provideSingleton(() => CoffeeBeansContainer(
        capacity: 1000,
        currentLoad: 1000,
        dispense: (amount) => CoffeeBeans(amount: amount)));
    provideSingleton(() => Heater());
    provideSingleton(() => Grinder());
  }
}
