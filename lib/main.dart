import 'package:flutter/material.dart';

import 'domain/models.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Espresso _espresso;

  void _makeEspresso() {
    setState(() {
      _espresso = Espresso();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _espresso == null? Container() : Text('Here is your espresso'),
            RaisedButton(
              key:Key('make_espresso_button'),
              onPressed: _makeEspresso,
              child: Text(
                'Make an Espresso',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
