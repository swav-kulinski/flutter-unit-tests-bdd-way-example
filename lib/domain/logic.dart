import 'package:flutter_playground_tdd/domain/models.dart';

class Heater {
  Water use(Water coldWater) => Water(amount: coldWater.amount,temperature: 100);
}

class Grinder {
  Caffeine use(CoffeeBeans beans) => Caffeine(amount: beans.amount);
}