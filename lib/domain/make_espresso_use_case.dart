import 'package:meta/meta.dart';

import 'logic.dart';
import 'models.dart';

class MakeEspressoUseCase {
  WaterContainer waterContainer;
  CoffeeBeansContainer coffeeBeansContainer;
  Heater heater;
  Grinder grinder;
  MakeEspressoUseCase({@required this.waterContainer, @required this.heater, @required this.coffeeBeansContainer, @required this.grinder});

  Future<Espresso> execute() async {
    final water = waterContainer.use(100);

    if(water.amount < 100) throw NoWaterException(); 

    final coffeeBeans = coffeeBeansContainer.use(20);

    if(coffeeBeans.amount < 20) throw NotEnoughCoffeeBeans();

    final caffeine = grinder.use(coffeeBeans);
    final hotWater = heater.use(water); 
    
    return Future<Espresso>(()=> Espresso(water: hotWater,caffine: caffeine));
  }
}

class NoWaterException implements Exception{}
class NotEnoughCoffeeBeans implements Exception{}