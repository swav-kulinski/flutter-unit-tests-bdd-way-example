import 'package:meta/meta.dart';

import 'make_espresso_use_case.dart';

class Espresso {
  final Caffeine caffine;
  final Water water;
  Espresso({@required this.water, @required this.caffine});
}

class Water {
  final int amount;
  final int temperature;
  Water({@required this.amount, this.temperature});
  Water.roomTemperature({@required amount})
      : this.amount = amount,
        this.temperature = 20;
}

class Caffeine{
  final int amount;
  Caffeine({@required this.amount});
}

class WaterContainer extends LimitedContainer<Water,NoWaterException>{
  WaterContainer({@required int capacity,@required int currentLoad,@required Water Function(int) dispense}):
  super(capacity:capacity,currentLoad:currentLoad,dispense:dispense);
}

class CoffeeBeansContainer extends LimitedContainer<CoffeeBeans,NotEnoughCoffeeBeans>{
  CoffeeBeansContainer({@required int capacity,@required int currentLoad,@required CoffeeBeans Function(int) dispense}):
  super(capacity:capacity,currentLoad:currentLoad,dispense:dispense);
}

abstract class LimitedContainer<T,E> {
  int _currentLoad;
  int get currentLoad => _currentLoad;
  final int capacity;
  final T Function(int) dispense;
  LimitedContainer({@required this.capacity,@required int currentLoad, @required this.dispense}):this._currentLoad = currentLoad;

  T use(int amount) {
    if(_isEnough(amount)) {
      _currentLoad -= amount;
      return dispense(amount);
    } else{
      final result = dispense(_currentLoad);
      _currentLoad = 0;
      return result;
    }
  }

  @protected bool _isEnough(int amount) => _currentLoad >= amount;

}

class CoffeeBeans {
  final int amount;
  CoffeeBeans({@required this.amount});
}
