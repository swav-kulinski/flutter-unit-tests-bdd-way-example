// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_playground_tdd/domain/models.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:flutter_playground_tdd/main.dart';

void main() {
  testWidgets('No Espresso made yet', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await _whenMyHomePageIsPumped(tester);
    expect(find.text('Make an Espresso'), findsOneWidget);
    expect(find.text('Here is your Espresso'), findsNothing);
  });

  testWidgets('Make Espresso', (WidgetTester tester) async {
    await _whenMyHomePageIsPumped(tester);
    await _whenButtonIsTapped(tester);
    await _whenMyHomePageIsPumped(tester);
    expect(find.text('Make an Espresso'), findsOneWidget);
    expect(find.text('Here is your espresso'), findsOneWidget);
  });
}

Future<void> _whenMyHomePageIsPumped(WidgetTester tester) async =>
    tester.pumpWidget(
      MaterialApp(
        home: MyHomePage(
          title: 'Espresso Maker',
        ),
      ),
    );

Future<void> _whenButtonIsTapped(WidgetTester tester) async => tester.tap(
      find.byKey(
        Key('make_espresso_button'),
      ),
    );

