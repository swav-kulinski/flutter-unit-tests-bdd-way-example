import 'package:flutter_playground_tdd/domain/make_espresso_use_case.dart';
import 'package:flutter_playground_tdd/domain/models.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'mocks.dart';

main() {
  test('returns espresso', () async {
    final subject = _givenEspressoUseCase();
    _givenSufficientWaterInTheContainer();
    _givenSufficientCoffeeInTheContainer();
    _givenHeaterIsUsed();
    _givenGrinderIsUsedWithInsufficientCoffeeBeans();
    final result = await _whenMakeEspressoUseCaseExecutes(subject);
    expect(result.runtimeType, Espresso);
    verify(mockCoffeeBeansContainer.use(20)).called(1);
    verify(mockWaterContainer.use(100)).called(1);
    verify(mockGrinder.use(stubSufficientCoffeeBeans)).called(1);
  });

  test('insufficient water', () async {
    final subject = _givenEspressoUseCase();
    _givenInsufficientWaterInTheContainer();

    try {
      await _whenMakeEspressoUseCaseExecutes(subject);
      fail('No exception thrown');
    } catch (e) {
      expect(e.runtimeType, NoWaterException);
    }
  });
  test('insufficient coffee beans', () async{
    final subject = _givenEspressoUseCase();
    _givenSufficientWaterInTheContainer();
    _givenInsufficientCoffeeInTheContainer();
    try {
      await _whenMakeEspressoUseCaseExecutes(subject);
      fail('No exception thrown');
    } catch(e) {
      expect(e.runtimeType, NotEnoughCoffeeBeans);
    }
  });
}

MakeEspressoUseCase _givenEspressoUseCase() =>
    MakeEspressoUseCase(waterContainer: mockWaterContainer, heater: mockHeater, coffeeBeansContainer:mockCoffeeBeansContainer, grinder: mockGrinder);

void _givenSufficientWaterInTheContainer() =>
    when(mockWaterContainer.use(100)).thenReturn(stubSufficientColdWater);

void _givenSufficientCoffeeInTheContainer() =>
  when(mockCoffeeBeansContainer.use(20)).thenReturn(stubSufficientCoffeeBeans);

void _givenInsufficientWaterInTheContainer() =>
    when(mockWaterContainer.use(100)).thenReturn(stubInsufficientColdWater);

void _givenInsufficientCoffeeInTheContainer() =>
    when(mockCoffeeBeansContainer.use(20)).thenReturn( stubInsufficientCoffeeBeans);

void _givenHeaterIsUsed() => when(mockHeater.use(stubSufficientColdWater))
    .thenReturn(stubSufficientHotWater);

void _givenGrinderIsUsedWithInsufficientCoffeeBeans() => when(mockGrinder.use(stubInsufficientCoffeeBeans)).thenReturn(stubInsufficientCaffeine);

Future<Espresso> _whenMakeEspressoUseCaseExecutes(
        MakeEspressoUseCase subject) =>
    subject.execute();
