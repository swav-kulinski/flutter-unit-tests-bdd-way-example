
import 'package:flutter_playground_tdd/domain/logic.dart';
import 'package:flutter_playground_tdd/domain/models.dart';
import 'package:mockito/mockito.dart';


class MockWaterContainer extends Mock implements WaterContainer {} 
final mockWaterContainer = MockWaterContainer();
class MockCoffeeBeansContainer extends Mock implements CoffeeBeansContainer{}
final mockCoffeeBeansContainer = MockCoffeeBeansContainer();
class MockHeater extends Mock implements Heater {}
final mockHeater = MockHeater();
class MockGrinder extends Mock implements Grinder{}
final mockGrinder = MockGrinder();

final stubWaterAmount = 100;
final stubSufficientColdWater = Water(amount:100,temperature:20);
final stubInsufficientColdWater = Water(amount:20,temperature:20);
final stubSufficientHotWater = Water(amount: 100, temperature: 100);
final stubSufficientCoffeeBeans = CoffeeBeans(amount:20);
final stubInsufficientCoffeeBeans = CoffeeBeans(amount: 10);
final stubSufficientCaffine = Caffeine(amount: 20);
final stubInsufficientCaffeine = Caffeine(amount: 10);
