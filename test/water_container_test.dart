import 'package:flutter_playground_tdd/domain/models.dart';
import 'package:flutter_test/flutter_test.dart';

import 'mocks.dart';

main() {
  test('get Water',(){
    final subject = WaterContainer(capacity: 1000,currentLoad: 1000,dispense: (amount) => Water.roomTemperature(amount: amount));
    final result = subject.use(stubWaterAmount);
    expect(result.amount, stubWaterAmount);
    expect(result.temperature,20);
    expect(subject.currentLoad,900);
  });

  test('get Water, not enought',(){
    final subject = WaterContainer(capacity: 1000, currentLoad: 90, dispense: (amount) => Water.roomTemperature(amount: amount));
    final result = subject.use(stubWaterAmount);
    expect(result.amount, 90);
    expect(result.temperature,20);
    expect(subject.currentLoad,0);
  });
}
