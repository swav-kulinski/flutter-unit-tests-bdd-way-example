import 'package:flutter_playground_tdd/domain/logic.dart';
import 'package:flutter_test/flutter_test.dart';

import 'mocks.dart';

main() {
  test('heat up water',(){
    final subject = Heater();
    final result = subject.use(stubSufficientColdWater);
    expect(result.amount, stubSufficientColdWater.amount);
    expect(result.temperature, 100);
  });
}