import 'package:flutter_playground_tdd/domain/models.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  test('Room temperature water',(){
  final subject = Water.roomTemperature(amount: 100);
  expect(subject.temperature, 20);

  });
}