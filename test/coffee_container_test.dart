import 'package:flutter_playground_tdd/domain/models.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  test('Get coffee beans',() {
    final subject = CoffeeBeansContainer(capacity: 1000,currentLoad: 1000,dispense: (amount) => CoffeeBeans(amount: amount));
    final result = subject.use(100);
    expect(result.runtimeType, CoffeeBeans);
    expect(result.amount,100);
    expect(subject.currentLoad, 900);
  });

  test('not enought coffee beans',(){
    final subject = CoffeeBeansContainer(capacity: 1000,currentLoad: 99,dispense: (amount) => CoffeeBeans(amount: amount));
    final result = subject.use(100);
    expect(result.runtimeType, CoffeeBeans);
    expect(result.amount,99);
    expect(subject.currentLoad, 0);
  });
}