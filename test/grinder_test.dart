import 'package:flutter_playground_tdd/domain/logic.dart';
import 'package:flutter_test/flutter_test.dart';

import 'mocks.dart';

main (){
  test('grind',(){
    final subject = Grinder();
    final result = subject.use(stubSufficientCoffeeBeans);
    expect(result.amount,20);
  });
}