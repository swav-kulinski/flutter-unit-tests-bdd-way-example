import 'package:flutter_playground_tdd/di/espresso_maker_module.dart';
import 'package:flutter_playground_tdd/domain/make_espresso_use_case.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  test('provider returns instance of MakeEspressoUseCase',(){
    final subject = EspressoMakerModule();
    final result = subject.makeEspressoUseCase;
    expect(result.runtimeType, MakeEspressoUseCase);
    expect(result.waterContainer, isNotNull);
    expect(result.coffeeBeansContainer, isNotNull);
    expect(result.grinder, isNotNull);
    expect(result.heater,isNotNull);
  });
}